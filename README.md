# Rate_of_return on investment

Business use case description and task definition:

Develop a deep-learning model to daily rank all stocks according to their potential return predictions.
Validate the model’s predictions with a “blind-set” that is including the most recent 20% of the data.
Expected evaluation metrics are as follows: (a) Pearson, Kendall, Spearman correlation coefficients
between scores that is given by the model for each day versus logarithmic returns of assets in that
day. (b) The average normalized distance (between predicted and ground-truth) of return rankings.
Optionally, propose and experiment with a basic investment strategy, which utilizes the ranking
model. (You can check whether the results are improved if you complete following imputation task)

The project is a demonstration on how to calculate the rate of return on investment, ranking companies based on ROR and logarithmic stock prediction